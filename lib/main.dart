import 'package:flight_booking_native_2/src/app/app.dart';
import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/object_factory.dart';
import 'package:flight_booking_native_2/src/utils/theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:provider/provider.dart';
import 'package:hive/hive.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Hive.openBox(Constants.BOX_NAME);
    runApp( ChangeNotifierProvider<ThemeNotifier>(
      create: (BuildContext context) {
        String theme = ObjectFactory().appHive.getCurrentTheme();
        print( "CurrentTheme" + theme.toString());
        if (theme == null ||
            theme == "" ||
            theme == "system default") {
          ObjectFactory().appHive.putCurrentTheme(theme: "system default");
          return ThemeNotifier(ThemeMode.system);
        }
        return ThemeNotifier(
            theme == "Dark" ? ThemeMode.dark : ThemeMode.light);
      },
      child: MyApp(),
    ),
    );
}

