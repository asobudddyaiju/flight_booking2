import 'package:flight_booking_native_2/src/app/app_theme.dart';
import 'package:flight_booking_native_2/src/ui/screens/destination_selection_from.dart';
import 'package:flight_booking_native_2/src/ui/screens/home_page.dart';
import 'package:flight_booking_native_2/src/ui/screens/results_page.dart';
import 'package:flight_booking_native_2/src/utils/theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:hive/hive.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Theme Demo',
      theme: AppTheme().lightTheme,
      darkTheme: AppTheme().darkTheme,
      themeMode: themeNotifier.getThemeMode(),

      home:ResultsPage()
    );
  }
}
