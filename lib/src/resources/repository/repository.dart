import 'package:flight_booking_native_2/src/models/login_request_model.dart';
import 'package:flight_booking_native_2/src/models/state.dart';
import 'package:flight_booking_native_2/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequest loginRequest}) =>
      UserApiProvider().loginCall(loginRequest);

}
