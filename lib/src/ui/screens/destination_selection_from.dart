import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'home_page.dart';

class DestinationFrom extends StatefulWidget {
 final bool destinationTo;

  DestinationFrom({this.destinationTo});

  @override
  _DestinationFromState createState() => _DestinationFromState();
}

class _DestinationFromState extends State<DestinationFrom> {
  TextEditingController _controller = TextEditingController();

  String imagePathString;
  int chooseStream;
  List<Color> colorArray = [
    Constants.kitGradients[2],
    Constants.kitGradients[3]
  ];
  Color codeColor;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Theme.of(context).scaffoldBackgroundColor,
      body: Column(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 5),
            color: Constants.kitGradients[0],
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    GestureDetector(
                        onTap: () {
                          pushAndRemoveUntil(context, HomePage(), false);
                        },
                        child: Container(
                            width: screenWidth(context, dividedBy: 10),
                            height: screenHeight(context, dividedBy: 26),
                            child: Icon(
                              Icons.clear,
                              color:Colors.white,
                              size: 25,
                            ))),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    Text(
                      widget.destinationTo == false
                          ? "From"
                          : "To",
                      style: TextStyle(
                          fontFamily: 'AirbnbCerealAppRegular',
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.1),
                  height: screenHeight(context, dividedBy: 15),
                  decoration: BoxDecoration(
                      color: Theme.of(context).inputDecorationTheme.fillColor,
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Row(
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1.3),
                          child: TextField(
                            style: TextStyle(color:Theme.of(context).textSelectionColor,fontSize: 18,
                                fontFamily: 'AirbnbCerealAppRegular',fontWeight: FontWeight.w500),
                            cursorColor: Constants.kitGradients[0],
                            maxLines: 1,
                            decoration: InputDecoration(
                              border: InputBorder.none,


                            ),
                            controller: _controller,
                            onChanged: (_) {

                            },
                          ),
                        ),
                        GestureDetector(
                          child: Icon(Icons.close,
                              size: 25,
                              color: Constants.kitGradients[0]),
                          onTap: () {
                            _controller.clear();
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          widget.destinationTo == false
              ? GestureDetector(
                  child: Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      SvgPicture.asset(
                          'assets/icons/location_icon.svg'),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Text(
                     "Current location",
                        style: TextStyle(
                            fontFamily: 'AirbnbCerealAppRegular',
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w700,
                            fontSize: 14,
                            color: Theme.of(context).textSelectionColor),
                      ),
                    ],
                  ),
                  onTap: () {

                  },
                )
              : Container(),
          widget.destinationTo == false
              ? SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                )
              : Container(),

        Expanded(

                            child: ListView.builder(
                                itemCount: 5,
                                itemBuilder: (BuildContext ctxt, int Index) {

                                  return GestureDetector(
                                          onTap: () async {

                                          },
                                          child: Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 12),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 30),
                                                        ),
                                                        SvgPicture.asset(
                                                            "assets/icons/city_icon.svg"),
                                                        SizedBox(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 30),
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width:
                                                                  screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          1.4),
                                                              child: RichText(
                                                                text: TextSpan(
                                                                    text: "London ",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'AirbnbCerealAppRegular',
                                                                        color: Theme.of(context).textSelectionColor,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w700,
                                                                        fontSize:
                                                                            16),
                                                                    children: <
                                                                        TextSpan>[
                                                                      TextSpan(
                                                                        text: "London",
                                                                        style: TextStyle(
                                                                            fontFamily:
                                                                                'AirbnbCerealAppRegular',
                                                                            fontWeight:
                                                                                FontWeight.w700,
                                                                            color: Theme.of(context).textSelectionColor,
                                                                            fontSize: 16),
                                                                      )
                                                                    ]),
                                                              ),
                                                            ),
                                                         Text(
                                                                    "New York" +
                                                                        ", " +
                                                                      "America" +
                                                                        ", " +
                                                                         "usa",
                                                                    style: TextStyle(
                                                                        fontFamily:
                                                                            'AirbnbCerealAppRegular',
                                                                        fontStyle:
                                                                            FontStyle
                                                                                .normal,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w400,
                                                                        fontSize:
                                                                            8,
                                                                        color: Constants
                                                                            .kitGradients[1]),
                                                                  ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 10.0),
                                                      child: Text(
                                                      "LON",
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'Montserrat',
                                                            fontStyle: FontStyle
                                                                .normal,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 12,
                                                            color: codeColor),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  height: 0.5,
                                                  width: screenWidth(context,
                                                      dividedBy: 1),
                                                  color: Theme.of(context).dividerColor,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                })),


        ],
      ),
    );
  }
}
