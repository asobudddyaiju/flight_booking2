import 'package:flight_booking_native_2/src/ui/widgets/build_button.dart';
import 'package:flight_booking_native_2/src/ui/widgets/color_switch_container.dart';
import 'package:flight_booking_native_2/src/ui/widgets/date_selection_box.dart';
import 'package:flight_booking_native_2/src/ui/widgets/destination_selection_box.dart';
import 'package:flight_booking_native_2/src/ui/widgets/home_modal_bottom_sheet.dart';
import 'package:flight_booking_native_2/src/ui/widgets/traveller_selection_box.dart';
import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/object_factory.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool oneWay = true;
  bool roundTrip = false;
  bool traveller = true;
  bool classSelect = true;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Column(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 2.5),
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.topRight,
                  colors: [
                    Constants.kitGradients[3],
                    Constants.kitGradients[0]
                  ],
                  tileMode: TileMode.repeated,
                ),
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 23),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Icon(
                        Icons.short_text_rounded,
                        color: Colors.white,
                        size: 40,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 4.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 80),
                            ),
                            Text(
                              "Search Flights",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'AirbnbCerealRegular',
                                  fontSize: 24,
                                  fontWeight: FontWeight.w500),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 100),
                            ),
                            DestinationSelectionBox(
                              heading: "From",
                              icon: "assets/icons/departure_icon.svg",
                              destinationShort: "CCG-",
                              destination: "Calicut.Intl",
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 20),
                                ),
                                Container(
                                  height: screenHeight(context, dividedBy: 40),
                                  width: 2,
                                  color: Constants.kitGradients[0],
                                ),
                              ],
                            ),
                            DestinationSelectionBox(
                              heading: "To",
                              icon: "assets/icons/arrival_icon.svg",
                              destinationShort: "CCG-",
                              destination: "Calicut.Intl",
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 15)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ColorSwitchContainer(
                        title: "OneWay",
                        onPressed: () {
                          setState(() {
                            oneWay = !oneWay;
                            roundTrip = !roundTrip;
                          });
                        },
                        oneWay: oneWay,
                      ),
                      ColorSwitchContainer(
                        title: "RoundTrip",
                        onPressed: () {
                          setState(() {
                            roundTrip = !roundTrip;
                            oneWay = !oneWay;
                          });
                        },
                        oneWay: roundTrip,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),

                  Row(
                    children: [
                      DateSelectionBox(
                        heading: "DEPARTURE",
                        date: "12 Nov",
                        year: "2020",
                      ),
                      roundTrip==true?DateSelectionBox(
                        heading: "RETURN",
                        date: "12 Nov",
                        year: "2020",
                        icon: false,
                      ):Container(width: screenWidth(context,dividedBy: 2.5),)
                    ],
                  ),
                  Divider(
                    thickness: 1,
                    color:Theme.of(context).dividerColor,
                  ),
                  TravellerSelectionBox(
                    icon: "assets/icons/person_icon.svg",
                    adultNo: 2,
                    childrenNo: 1,
                    infant: 1,
                    heading: "TRAVELLER",
                    traveller: true,
                    onPressed: () {
                      setState(() {
                        traveller = !traveller;
                      });
                    },
                    arrowUpward: traveller,
                  ),
                  TravellerSelectionBox(
                    icon: "assets/icons/chair_icon.svg",
                    heading: "CLASS",
                    subHeading: ObjectFactory().appHive.getSelectedClass()==null?"Economy":ObjectFactory().appHive.getSelectedClass(),
                    onPressed: () {
                      setState(() {
                        classSelect = !classSelect;
                      });
                    },
                    arrowUpward: classSelect,
                    bottomSheet: (){
                      classSelectBottomSheet(context);
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 20),
            ),
            BuildButton(
              title: "Search",
              onPressed: (){},
            )
          ],
        ));
  }

  classSelectBottomSheet(context) {
    showModalBottomSheet(
      isDismissible: true,
        isScrollControlled: true,
        context: context,
        builder: (context) =>HomeModalBottomSheet() );
  }
}
