
import 'package:flight_booking_native_2/src/ui/widgets/result_shimmer_body.dart';
import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';


class ResultShrimmer extends StatefulWidget {
  bool roundTripStatus;
  String route, dateTimePassengers;

  ResultShrimmer({this.roundTripStatus, this.dateTimePassengers, this.route});

  @override
  _ResultShrimmerState createState() => _ResultShrimmerState();
}

class _ResultShrimmerState extends State<ResultShrimmer> {
  ScrollController _controller = ScrollController();
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[7],
      body: Column(
        children: [
          Container(
            width: screenWidth(context,dividedBy: 1),
            height: screenHeight(context,dividedBy: 5),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  Constants.kitGradients[3],
                  Constants.kitGradients[0]
                ],
                tileMode: TileMode.repeated,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: screenHeight(context,dividedBy:14),),

                  Icon(Icons.arrow_back_ios_outlined,size: 20,),

                  SizedBox(height: screenHeight(context,dividedBy: 40),),

                  RichText(
                    text: TextSpan(
                      text: 'Kannur' + "-",
                      style: TextStyle(color: Colors.white,fontFamily:'AirbnbCerealAppRegular',fontSize:20,fontWeight: FontWeight.w700 ),
                      children: <TextSpan>[
                        TextSpan(text: "Kozhikode",style:TextStyle(color: Colors.white,fontFamily:'AirbnbCerealAppRegular',fontSize:20,fontWeight: FontWeight.w700 ),),

                      ],
                    ),
                  ),

                  RichText(
                    text: TextSpan(
                      text: 'Wed 3 Nov,' ,
                      style: TextStyle(color: Constants.kitGradients[1],fontFamily:'AirbnbCerealAppRegular',fontSize:12,fontWeight: FontWeight.w400 ),
                      children: <TextSpan>[
                        TextSpan(text: "3 " + "Passengers",style:TextStyle(color: Constants.kitGradients[1],fontFamily:'AirbnbCerealAppRegular',fontSize:12,fontWeight: FontWeight.w400 ),),

                      ],
                    ),
                  ),






                ],
              ),
            ),
          ),

          // Container(
          //   width: screenWidth(context, dividedBy: 1),
          //   height: 0.3,
          //   color: Constants.kitGradients[4],
          // ),
          Container(
            height: screenHeight(context, dividedBy: 17),
            width: screenWidth(context,dividedBy: 1),
            color: Constants.kitGradients[5],
            child: Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              enabled: _enabled,
              child: ListView.builder(
                  controller: _controller,
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (BuildContext context, index) {
                    return Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: screenWidth(context, dividedBy: 4),
                            decoration: BoxDecoration(
                              color: Constants.kitGradients[2],
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: screenWidth(context, dividedBy: 6),
                            decoration: BoxDecoration(
                              color: Constants.kitGradients[2],
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                          ),
                        ),

                      ],
                    );
                  }),
            ),
          ),

          Expanded(
            child: ListView.builder(
                controller: _controller,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: 6,
                itemBuilder: (BuildContext context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[6],
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ResultShimmerBody(),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
