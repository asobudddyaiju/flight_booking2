import 'package:flight_booking_native_2/src/ui/widgets/filter_button.dart';
import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flight_booking_native_2/src/ui/widgets/resultspagetile.dart';
class ResultsPage extends StatefulWidget {
  @override
  _ResultsPageState createState() => _ResultsPageState();
}

class _ResultsPageState extends State<ResultsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [

          Container(
            width: screenWidth(context,dividedBy: 1),
            height: screenHeight(context,dividedBy: 5),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  Constants.kitGradients[3],
                  Constants.kitGradients[0]
                ],
                tileMode: TileMode.repeated,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: screenHeight(context,dividedBy:14),),

                  Icon(Icons.arrow_back_ios_outlined,size: 20,color: Colors.white,),

                  SizedBox(height: screenHeight(context,dividedBy: 40),),

                  RichText(
                    text: TextSpan(
                      text: 'Kannur' + "-",
                      style: TextStyle(color: Colors.white,fontFamily:'AirbnbCerealAppRegular',fontSize:20,fontWeight: FontWeight.w700 ),
                      children: <TextSpan>[
                        TextSpan(text: "Kozhikode",style:TextStyle(color: Colors.white,fontFamily:'AirbnbCerealAppRegular',fontSize:20,fontWeight: FontWeight.w700 ),),

                      ],
                    ),
                  ),

                  RichText(
                    text: TextSpan(
                      text: 'Wed 3 Nov,' ,
                      style: TextStyle(color: Constants.kitGradients[2],fontFamily:'AirbnbCerealAppRegular',fontSize:12,fontWeight: FontWeight.w400 ),
                      children: <TextSpan>[
                        TextSpan(text: "3 " + "Passengers",style:TextStyle(color: Constants.kitGradients[2],fontFamily:'AirbnbCerealAppRegular',fontSize:12,fontWeight: FontWeight.w400 ),),

                      ],
                    ),
                  ),






                ],
              ),
            ),
          ),
          SizedBox(height: screenHeight(context,dividedBy: 80),),

          Container(
            height: screenHeight(context,dividedBy: 25),

            child: ListView.builder(itemCount:5,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder:(BuildContext context, int index){

                  return FilterButton(title: "Stops",onPressed: (){},);

                }),
          ),

          SizedBox(height: screenHeight(context,dividedBy: 80),),

          Container(
            color: Constants.kitGradients[2],
            height: screenHeight(context,dividedBy: 1.363),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount:5,itemBuilder: (BuildContext context, int index){

              return ResultsPageTile(
                time: "17:10",
                airportName: "CNN",
                totalTime1: "29hr 1min",
                oneWayTime2: "22:00",
                oneWayAirportName2: "CCJ",
                price: "110",
                 oneWay: false,

              );

            }),
          ),




        ],
      ),
    );
  }
}
