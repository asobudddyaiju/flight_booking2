import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class AddSubCountWidget extends StatefulWidget {
  final  int count;
  final String heading;
  final String subHeading;
  ValueChanged onValueChanged;
  AddSubCountWidget({this.count,this.onValueChanged,this.subHeading,this.heading});
  @override
  _AddSubCountWidgetState createState() => _AddSubCountWidgetState();
}

class _AddSubCountWidgetState extends State<AddSubCountWidget> {
  int counter;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 100),
        ),

        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Container(
              width: screenWidth(context, dividedBy: 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.heading,
                    style: TextStyle(fontFamily: 'AirbnbCerealRegular',
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: Constants.kitGradients[4],),),

                  Text(widget.subHeading,
                    style: TextStyle(fontFamily: 'AirbnbCerealRegular',
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w300,
                      fontSize: 12,
                      color: Constants.kitGradients[1],),),
                ],
              ),
            ),


            SizedBox(width: screenWidth(context, dividedBy: 4,),),

            Column(
              children: [

                SizedBox(height: 3,),
                Row(
                  children: [
                    GestureDetector(
                        onTap: () {
                          counter=widget.count;
                          subtractCount(widget.count);
                          widget.onValueChanged(counter);
                          },
                        child: Icon(Icons.remove_circle,size: 30,color: Constants.kitGradients[0],))


                    ,Container(
                        width: screenWidth(context, dividedBy: 9),
                        child: Center(child: Text(widget.count.toString(),
                          style: TextStyle(color: Constants.kitGradients[4],
                              fontSize: 18),))),

                    SizedBox(height: screenHeight(context, dividedBy: 150),),
                    GestureDetector(
                      onTap: () {
                        counter=widget.count;
                        addCount(widget.count);
                        widget.onValueChanged(counter);
                      },
                      child: Icon(Icons.add_circle,size: 30,color: Constants.kitGradients[0],))


                  ],
                ),
              ],
            ),


          ],
        ),

        SizedBox(
          height: screenHeight(context, dividedBy: 100),
        ),
      ],
    );
  }

  void addCount(int sum) {
    sum = sum + 1;
    setState(() {
      counter = sum;
    });
  }

  void subtractCount(int sum) {
    if (sum > 0) {
      sum = sum - 1;
      setState(() {
        counter = sum;
      });
    }
  }
}
