import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
class BuildButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  BuildButton({this.title,this.onPressed});
  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,dividedBy: 1.15),
      height: screenHeight(context,dividedBy: 17),
      child: RaisedButton(
        color:Constants.kitGradients[0],
        onPressed: (){
          widget.onPressed();
        },

        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(2),
        ),
        child: Center(
          child: Text(widget.title,style: TextStyle(color:Colors.white,fontFamily: 'AirbnbCerealRegular',
              fontSize:20,fontWeight: FontWeight.w700  ),),
        ),
      ),
    );;
  }
}

