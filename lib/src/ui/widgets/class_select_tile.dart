import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ClassSelectTile extends StatefulWidget {
  final String heading;
  final int index;
  final ValueChanged onValueChanged;
  final ValueChanged onChangedHeading;

  final int selectedValue;
  ClassSelectTile({this.heading,this.index,this.onValueChanged,this.selectedValue,this.onChangedHeading}) ;
  @override
  _ClassSelectTileState createState() => _ClassSelectTileState();
}

class _ClassSelectTileState extends State<ClassSelectTile> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: screenHeight(context,dividedBy: 80)),
      child: Row(
        children: [

          Container(
            width: screenWidth(context, dividedBy: 1.3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.heading,
                  style: TextStyle(fontFamily: 'AirbnbCerealRegular',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: Constants.kitGradients[4],),),

              ],
            ),
          ),

          GestureDetector(

            onTap: (){
              count=widget.index;
              widget.onValueChanged(count);
              widget.onChangedHeading(widget.heading);
              print(widget.index);

            },
              child:widget.selectedValue==widget.index? SvgPicture.asset("assets/icons/radio_button_yes.svg"):SvgPicture.asset("assets/icons/radio_button_no.svg"))


        ],
      ),
    );
  }
}
