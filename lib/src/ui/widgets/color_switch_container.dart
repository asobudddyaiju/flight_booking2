import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
class ColorSwitchContainer extends StatefulWidget {
  final String title;
  final bool oneWay;
  Function onPressed;
  ColorSwitchContainer({this.title,this.oneWay,this.onPressed});
  @override
  _ColorSwitchContainerState createState() => _ColorSwitchContainerState();
}

class _ColorSwitchContainerState extends State<ColorSwitchContainer> {
  bool onClick;
  @override
  Widget build(BuildContext context) {
    return Container(
             width: screenWidth(context,dividedBy: 2.31),
      child: RaisedButton(
        color: widget.oneWay==true?Constants.kitGradients[0]:Theme.of(context).buttonColor,
        onPressed: (){
          widget.onPressed();
        },

        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(2),
        ),
        child: Center(
          child: Text(widget.title,style: TextStyle(color:widget.oneWay==true?Colors.white:Constants.kitGradients[1],fontFamily: 'AirbnbCerealRegular',
              fontSize:15,fontWeight: FontWeight.w700  ),),
        ),
      ),
    );
  }
}
