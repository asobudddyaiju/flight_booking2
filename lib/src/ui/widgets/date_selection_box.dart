import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DateSelectionBox extends StatefulWidget {
  final String heading;
  final String date;
  final String year;
  final bool icon;

  DateSelectionBox({this.date,this.heading,this.year,this.icon});
  @override
  _DateSelectionBoxState createState() => _DateSelectionBoxState();
}

class _DateSelectionBoxState extends State<DateSelectionBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context,dividedBy:13 ),
        width: screenWidth(context,dividedBy: 2.31),

         child: Padding(
           padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 40)),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.start,

             children: [

               widget.icon==false?Container(width: screenWidth(context,dividedBy: 20),):SvgPicture.asset("assets/icons/calendar_icon.svg",),



               Padding(
                 padding: EdgeInsets.symmetric(horizontal:screenWidth(context,dividedBy: 40) ),
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [

                     Text(widget.heading,style: TextStyle(color:Constants.kitGradients[1],fontFamily: 'AirbnbCerealRegular',
                         fontSize:12,fontWeight: FontWeight.w400  ),),

                     RichText(
                       text: TextSpan(
                         text:widget.date + " ",
                         style: TextStyle(color:Theme.of(context).textSelectionColor,fontFamily: 'AirbnbCerealRegular',fontWeight: FontWeight.w500,fontSize: 16),
                         children: <TextSpan>[
                           TextSpan(text: widget.year, style: TextStyle(color:Theme.of(context).textSelectionColor,fontFamily:
                           'AirbnbCerealRegular',fontWeight: FontWeight.w300,fontSize: 16),),
                         ],
                       ),
                     ),




                   ],
                 ),
               ),



             ],

           ),
         ),


    );
  }
}
