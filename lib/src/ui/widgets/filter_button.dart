import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
class FilterButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  FilterButton({this.onPressed,this.title});
  @override
  _FilterButtonState createState() => _FilterButtonState();
}

class _FilterButtonState extends State<FilterButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 50)),
      child: RaisedButton(onPressed: (){
        widget.onPressed();
      },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),

      child: Center(

          child: Text("Stops",style: TextStyle(color:Constants.kitGradients[4],fontFamily: 'Roboto',fontSize: 14,fontWeight: FontWeight.w700),)),),
    );
  }
}
