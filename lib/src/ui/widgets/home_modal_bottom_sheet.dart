import 'package:flight_booking_native_2/src/ui/screens/home_page.dart';
import 'package:flight_booking_native_2/src/ui/widgets/add_sub_count_widget.dart';
import 'package:flight_booking_native_2/src/ui/widgets/build_button.dart';
import 'package:flight_booking_native_2/src/ui/widgets/class_select_tile.dart';
import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/object_factory.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';

class HomeModalBottomSheet extends StatefulWidget {
  @override
  _HomeModalBottomSheetState createState() => _HomeModalBottomSheetState();
}

class _HomeModalBottomSheetState extends State<HomeModalBottomSheet> {
  List<String> className = [
    "Economy",
    "Premium Economy",
    "Business",
    "First Class"
  ];
  int adultCount = 0;
  int childrenCount = 0;
  int infantCount = 0;
  int selectedTabNum = 0;
  String selectedClass;
  @override
  void initState() {
    ObjectFactory().appHive.getAdult() == null
        ? adultCount = 1
        : adultCount = int.parse(ObjectFactory().appHive.getAdult());
    ObjectFactory().appHive.getChildren() == null
        ? childrenCount = 0
        : childrenCount = int.parse(ObjectFactory().appHive.getChildren());
    ObjectFactory().appHive.getInfant() == null
        ? infantCount = 0
        : infantCount = int.parse(ObjectFactory().appHive.getInfant());
    ObjectFactory().appHive.getSelectedClassTab() == null
        ? selectedTabNum = 0
        : selectedTabNum =
            int.parse(ObjectFactory().appHive.getSelectedClassTab());

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 1.67),
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 15)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Text(
              'Passengers',
              style: TextStyle(
                  fontFamily: 'AirbnbCerealRegular',
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                  color: Constants.kitGradients[0]),
            ),
            AddSubCountWidget(
              count: adultCount,
              onValueChanged: (value) {
                setState(() {
                  adultCount = value;
                });
              },
              heading: "Adults",
              subHeading: "Under 2",
            ),
            AddSubCountWidget(
              count: childrenCount,
              onValueChanged: (value) {
                setState(() {
                  childrenCount = value;
                });
              },
              heading: "Children",
              subHeading: "Under 3",
            ),
            AddSubCountWidget(
              count: infantCount,
              onValueChanged: (value) {
                setState(() {
                  infantCount = value;
                });
              },
              heading: "Infant",
              subHeading: "Under 2",
            ),
            Divider(
              thickness: 1,
              color: Constants.kitGradients[2],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 150),
            ),
            Text(
              'Classes',
              style: TextStyle(
                  fontFamily: 'AirbnbCerealRegular',
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                  color: Constants.kitGradients[0]),
            ),
            ListView.builder(
                itemCount: 4,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return ClassSelectTile(
                    heading: className[index],
                    onValueChanged: (value) {
                      setState(() {
                        selectedTabNum = value;
                      });
                    },
                    index: index,
                    selectedValue: selectedTabNum,
                    onChangedHeading: (value) {
                      selectedClass = value;
                    },
                  );
                }),
            SizedBox(
              height: screenHeight(context, dividedBy: 120),
            ),
            BuildButton(
              title: "NEXT",
              onPressed: () {
                ObjectFactory().appHive.putAdult(adultCount.toString());
                ObjectFactory().appHive.putChildren(childrenCount.toString());
                ObjectFactory().appHive.putInfant(infantCount.toString());
                ObjectFactory()
                    .appHive
                    .putSelectedClass(selectedClass)
                    .toString();
                ObjectFactory()
                    .appHive
                    .putSelectedClassTab(selectedTabNum.toString());

                push(context, HomePage());
              },
            )
          ],
        ),
      ),
    );
  }
}
