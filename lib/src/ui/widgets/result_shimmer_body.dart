
import 'package:flight_booking_native_2/src/ui/widgets/shimmer%20_body.dart';
import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ResultShimmerBody extends StatefulWidget {
  @override
  _ResultShimmerBodyState createState() => _ResultShimmerBodyState();
}

class _ResultShimmerBodyState extends State<ResultShimmerBody> {
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 50)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    enabled: _enabled,
                    child: Container(
                      width: screenWidth(context, dividedBy: 14),
                      height: screenWidth(context, dividedBy: 14),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[3],
                          borderRadius: BorderRadius.circular(6.0)),
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 60),
                  ),
                  Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    enabled: _enabled,
                    child: Container(
                      width: screenWidth(context, dividedBy: 14),
                      height: screenWidth(context, dividedBy: 14),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[3],
                          borderRadius: BorderRadius.circular(6.0)),
                    ),
                  ),
                  // Column(
                  //   children: [
                  //     Row(
                  //       children: [
                  //         Shimmer.fromColors(
                  //           baseColor: Colors.grey[300],
                  //           highlightColor: Colors.grey[100],
                  //           enabled: _enabled,
                  //           child: Container(
                  //             width: screenWidth(context, dividedBy: 4),
                  //             height: screenHeight(context, dividedBy: 32),
                  //             decoration: BoxDecoration(
                  //                 color: Constants.kitGradients[2],
                  //                 borderRadius: BorderRadius.circular(6.0)),
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           width: screenWidth(context, dividedBy: 3.2),
                  //         ),
                  //         Shimmer.fromColors(
                  //           baseColor: Colors.grey[300],
                  //           highlightColor: Colors.grey[100],
                  //           enabled: _enabled,
                  //           child: Container(
                  //             width: screenWidth(context, dividedBy: 6.2),
                  //             height: screenHeight(context, dividedBy: 32),
                  //             decoration: BoxDecoration(
                  //                 color: Constants.kitGradients[2],
                  //                 borderRadius: BorderRadius.circular(6.0)),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //     SizedBox(
                  //       height: screenHeight(context, dividedBy: 100),
                  //     ),
                  //     Row(
                  //       children: [
                  //         Shimmer.fromColors(
                  //           baseColor: Colors.grey[300],
                  //           highlightColor: Colors.grey[100],
                  //           enabled: _enabled,
                  //           child: Container(
                  //             width: screenWidth(context, dividedBy: 4),
                  //             height: screenHeight(context, dividedBy: 46),
                  //             decoration: BoxDecoration(
                  //                 color: Constants.kitGradients[2],
                  //                 borderRadius: BorderRadius.circular(3.0)),
                  //           ),
                  //         ),
                  //         SizedBox(
                  //           width: screenWidth(context, dividedBy: 3.5),
                  //         ),
                  //         Shimmer.fromColors(
                  //           baseColor: Colors.grey[300],
                  //           highlightColor: Colors.grey[100],
                  //           enabled: _enabled,
                  //           child: Container(
                  //             width: screenWidth(context, dividedBy: 5.2),
                  //             height: screenHeight(context, dividedBy: 46),
                  //             decoration: BoxDecoration(
                  //                 color: Constants.kitGradients[2],
                  //                 borderRadius: BorderRadius.circular(3.0)),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //     SizedBox(
                  //       height: screenHeight(context, dividedBy: 100),
                  //     ),
                  //     Row(
                  //       children: [
                  //         SizedBox(
                  //           width: screenWidth(context, dividedBy: 1.7),
                  //         ),
                  //         Shimmer.fromColors(
                  //           baseColor: Colors.grey[300],
                  //           highlightColor: Colors.grey[100],
                  //           enabled: _enabled,
                  //           child: Container(
                  //             width: screenWidth(context, dividedBy: 7.5),
                  //             height: screenHeight(context, dividedBy: 32),
                  //             decoration: BoxDecoration(
                  //                 color: Constants.kitGradients[3],
                  //                 borderRadius: BorderRadius.circular(6.0)),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ],
                  // )
                ],
              ),
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: _enabled,
                child: Container(
                  width: screenWidth(context, dividedBy: 4),
                  height: screenHeight(context, dividedBy: 60),
                  decoration: BoxDecoration(
                      color: Constants.kitGradients[2],
                      borderRadius: BorderRadius.circular(3.0)),
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context,dividedBy: 1000),
          ),
          Padding(
            padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 50)),
            child: Divider(color: Constants.kitGradients[8],thickness: 1,),
          ),
          SizedBox(
            height: screenHeight(context,dividedBy: 50),
          ),
          ShimmerBody()
        ],
      ),
    );
  }
}
