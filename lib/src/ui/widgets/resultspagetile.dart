import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ResultsPageTile extends StatefulWidget {
  final String time;
  final String airportName;
  final String totalTime1;
  final String oneWayTime2;
  final String oneWayAirportName2;
  final String price;
  final bool oneWay;
  ResultsPageTile(
      {this.time,
      this.airportName,
      this.totalTime1,
      this.oneWayTime2,
      this.oneWayAirportName2,this.price,this.oneWay});
  @override
  _ResultsPageTileState createState() => _ResultsPageTileState();
}

class _ResultsPageTileState extends State<ResultsPageTile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 40)),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        color: Colors.white,
        child: Container(

          height: widget.oneWay==true?screenHeight(context, dividedBy: 6):screenHeight(context,dividedBy: 4),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                widget.oneWay==true?Row(
                  children: [
                    SvgPicture.asset("assets/icons/plus.svg"),
                    SizedBox(
                      width: 4.0,
                    ),
                    SvgPicture.asset("assets/icons/plus.svg"),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 2.3),
                    ),
                    Text(
                      "1 offers from kiwi.com",
                      style: TextStyle(
                          color: Constants.kitGradients[1],
                          fontFamily: 'ProximaNovaRegular',
                          fontSize: screenWidth(context, dividedBy: 40),
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ):Row(
                  children: [
                    SvgPicture.asset("assets/icons/plus.svg"),
                    SizedBox(
                      width: 4.0,
                    ),
                    SvgPicture.asset("assets/icons/plus.svg"),
                    SizedBox(
                      width: 4.0,
                    ),
                    SvgPicture.asset("assets/icons/plus.svg"),

                    SizedBox(
                      width: screenWidth(context, dividedBy: 2.8),
                    ),
                    Text(
                      "1 offers from kiwi.com",
                      style: TextStyle(
                          color: Constants.kitGradients[1],
                          fontFamily: 'ProximaNovaRegular',
                          fontSize: screenWidth(context, dividedBy: 40),
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Divider(
                  thickness: 1,
                  color: Constants.kitGradients[2],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: screenWidth(context,dividedBy: 7),
                      child: Column(
                        children: [
                          Text(
                            widget.time,
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w700),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 100),
                          ),
                          Text(
                            widget.airportName,
                            style: TextStyle(
                                fontSize: 14,
                                color: Constants.kitGradients[1],
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),

                    Container(
                      width: screenWidth(context,dividedBy: 6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            widget.time,
                            style: TextStyle(
                                fontSize: 10,
                                color: Colors.black,
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w400),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            size: 10,
                            color: Constants.kitGradients[1],
                          ),
                          Text(
                            widget.totalTime1,
                            style: TextStyle(
                                fontSize: 10,
                                color: Constants.kitGradients[1],
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: screenWidth(context,dividedBy: 7),
                      child: Column(

                        children: [
                          Text(
                            widget.oneWayTime2,
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w700),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 100),
                          ),
                          Text(
                            widget.oneWayAirportName2,
                            style: TextStyle(
                                fontSize: 14,
                                color: Constants.kitGradients[1],
                                fontFamily: 'AirbnbCerealAppRegular',
                                fontWeight: FontWeight.w400),
                          ),
                        ],
                      ),
                    ),

                    widget.oneWay==true?Container(
                      width: screenWidth(context,dividedBy: 7),
                      child: Center(
                        child: Text(
                          "\u0024" + widget.price,
                          style: TextStyle(
                              fontSize: 20,
                              color: Constants.kitGradients[0],
                              fontFamily: 'AirbnbCerealAppRegular',
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ):Container(width: screenWidth(context,dividedBy: 7),)

                  ],
                ),

                widget.oneWay==false?Column(
                  children: [
                    SizedBox(height: screenHeight(context,dividedBy: 40),),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: screenWidth(context,dividedBy: 7),
                          child: Column(
                            children: [
                              Text(
                                widget.time,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              Text(
                                widget.airportName,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Constants.kitGradients[1],
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),

                        Container(
                          width: screenWidth(context,dividedBy: 6),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                widget.time,
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                size: 10,
                                color: Constants.kitGradients[1],
                              ),
                              Text(
                                widget.totalTime1,
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Constants.kitGradients[1],
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: screenWidth(context,dividedBy: 7),
                          child: Column(

                            children: [
                              Text(
                                widget.oneWayTime2,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w700),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 100),
                              ),
                              Text(
                                widget.oneWayAirportName2,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Constants.kitGradients[1],
                                    fontFamily: 'AirbnbCerealAppRegular',
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),

                        Container(
                          width: screenWidth(context,dividedBy: 7),
                          child: Center(
                            child: Text(
                              "\u0024" + widget.price,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Constants.kitGradients[0],
                                  fontFamily: 'AirbnbCerealAppRegular',
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        )

                      ],
                    ),
                  ],
                ):Container()


              ],
            ),
          ),
        ),
      ),
    );
  }
}
