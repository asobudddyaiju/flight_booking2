import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerBody extends StatefulWidget {
  @override
  _ShimmerBodyState createState() => _ShimmerBodyState();
}

class _ShimmerBodyState extends State<ShimmerBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 20),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context,dividedBy: 80),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),

              ],
            ),
            Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),
                Icon(Icons.arrow_forward_outlined,color: Colors.black,size: screenWidth(context,dividedBy: 30),),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),

              ],
            ),
            Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 20),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context,dividedBy: 80),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),

              ],
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 10),
              height: screenWidth(context, dividedBy: 15),
            )
          ],
        ),
        SizedBox(
          height: screenHeight(context,dividedBy: 50),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 20),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context,dividedBy: 80),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),

              ],
            ),
            Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),
                Icon(Icons.arrow_forward_outlined,color: Colors.black,size: screenWidth(context,dividedBy: 30),),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),

              ],
            ),
            Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 20),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context,dividedBy: 80),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  enabled: true,
                  child: Container(
                    width: screenWidth(context, dividedBy: 10),
                    height: screenWidth(context, dividedBy: 30),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[9],
                        borderRadius: BorderRadius.circular(6.0)),
                  ),
                ),

              ],
            ),
            Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              enabled: true,
              child: Container(
                width: screenWidth(context, dividedBy: 10),
                height: screenWidth(context, dividedBy: 15),
                decoration: BoxDecoration(
                    color: Constants.kitGradients[9],
                    borderRadius: BorderRadius.circular(6.0)),
              ),
            )
          ],
        ),
      ],
    );
  }
}
