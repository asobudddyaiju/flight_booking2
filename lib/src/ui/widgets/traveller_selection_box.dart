import 'package:flight_booking_native_2/src/utils/constants.dart';
import 'package:flight_booking_native_2/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TravellerSelectionBox extends StatefulWidget {
  final String icon;
  final int adultNo;
  final int childrenNo;
  final int infant;
  final String heading;
  final bool traveller;
  final String subHeading;
  final bool arrowUpward;
  Function onPressed;
  Function bottomSheet;

  TravellerSelectionBox(
      {this.icon,
      this.adultNo,
      this.childrenNo,
      this.infant,
      this.heading,
      this.traveller,
      this.subHeading,
      this.onPressed,this.arrowUpward,this.bottomSheet});
  @override
  _TravellerSelectionBoxState createState() => _TravellerSelectionBoxState();
}

class _TravellerSelectionBoxState extends State<TravellerSelectionBox> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: screenHeight(context,dividedBy: 50),),
        Container(
          width: screenWidth(context, dividedBy: 1.1),
          height: screenHeight(context, dividedBy: 13),
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 30)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    SvgPicture.asset(widget.icon),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: widget.traveller == true
                          ? screenWidth(context, dividedBy: 40)
                          : screenWidth(context, dividedBy: 30)),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.57),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.heading,
                          style: TextStyle(
                              color: Constants.kitGradients[1],
                              fontFamily: 'AirbnbCerealRegular',
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                        widget.traveller == true
                            ? RichText(
                                text: TextSpan(
                                  text: widget.adultNo.toString() +
                                      " Adult, " +
                                      widget.childrenNo.toString() +
                                      " Children, " +
                                      widget.infant.toString() +
                                      " Infants",
                                  style: TextStyle(
                                      color: Theme.of(context).textSelectionColor,
                                      fontFamily: 'AirbnbCerealRegular',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16),
                                  children: <TextSpan>[],
                                ),
                              )
                            : RichText(
                                text: TextSpan(
                                  text: widget.subHeading,
                                  style: TextStyle(
                                      color: Theme.of(context).textSelectionColor,
                                      fontFamily: 'AirbnbCerealRegular',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16),
                                  children: <TextSpan>[],
                                ),
                              )
                      ],
                    ),
                  ),
                ),
                Column(
                  children: [

                    SizedBox(height: screenHeight(context,dividedBy:30 ),),
                    GestureDetector(
                      onTap: (){
                        widget.onPressed();
                        if(widget.arrowUpward==true)
                        widget.bottomSheet();
                      },
                      child: widget.arrowUpward==true?SvgPicture.asset(
                        "assets/icons/arrow_down.svg",width: screenWidth(context,dividedBy: 25,),
                        color: Constants.kitGradients[0],
                      ):SvgPicture.asset(
                        "assets/icons/arrow_upward.svg",width: screenWidth(context,dividedBy: 25,),
                        color: Constants.kitGradients[0],
                      )
                    ),
                  ],
                ),

              ],
            ),
          ),
        ),

        Divider(thickness: 1,color: Theme.of(context).dividerColor,),
      ],
    );
  }
}
