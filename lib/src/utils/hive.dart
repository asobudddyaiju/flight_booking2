import 'package:hive/hive.dart';
import 'constants.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _ADULT = "adult";
  static const String _PREFERENCE = "preferenceid";
  static const String _LATITUDE = "longitude";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _CURRENTTHEME = "currenttheme";
  static const String _CHILDREN = "userpassword";
  static const String _INFANT = "infant";
  static const String _SELECTEDClASS = "class";
  static const String _SELECTEDCLASSTAB = "classtab";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }


  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }


  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  String getAdult() {
    return hiveGet(key: _ADULT);
  }

  putAdult(String value) {
    return hivePut(key: _ADULT, value: value);
  }

  putCurrentTheme({String theme}) {
    hivePut(key: _CURRENTTHEME, value: theme);
  }

  String getCurrentTheme() {
    return hiveGet(key: _CURRENTTHEME);
  }

  String getChildren() {
    return hiveGet(key: _CHILDREN);
  }

  putChildren(String value) {
    return hivePut(key: _CHILDREN, value: value);
  }

  String getInfant() {
    return hiveGet(key: _INFANT);
  }

  putInfant(String value) {
    return hivePut(key: _INFANT, value: value);
  }

  String getSelectedClass() {
    return hiveGet(key: _SELECTEDClASS);
  }

  putSelectedClass(String value) {
    return hivePut(key: _SELECTEDClASS, value: value);
  }
  String getSelectedClassTab() {
    return hiveGet(key: _SELECTEDCLASSTAB);
  }

  putSelectedClassTab(String value) {
    return hivePut(key: _SELECTEDCLASSTAB, value: value);
  }


  AppHive();

}
